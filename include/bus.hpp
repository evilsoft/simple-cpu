#ifndef BUS_HPP
#define BUS_HPP

#include "helpers.hpp"
#include "cpu.hpp"
#include "rom.hpp"

class Bus {
  public:
    Bus();
    ~Bus();

  private:
    Cpu* cpu;
    Rom* rom;
    array<data_t, 16 * 0x1000> ram;

    addr_t romStart;

  public:
    CpuRegisters ReadCpuRegisters();

    data_t ReadFrom(addr_t addr);
    Bus* WriteTo(addr_t addr, data_t data);

    bool Tick();
};

#endif
