#ifndef HELPERS_HPP
#define HELPERS_HPP

// clib includes
#include <array>
#include <cstdint>
#include <cstring>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>

using namespace std;

// Macros
#define RESET_IO dec << setw(0) << setfill(' ')
#define HEX(x) setw(2) << setfill('0') << hex << +x << setw(0) << RESET_IO
#define HEX_ADDR(x) setw(4) << setfill('0') << hex << +x << RESET_IO

// Type Defs
typedef uint8_t data_t;
typedef uint16_t addr_t;

int hexStringToInteger(string hexString);

#endif
