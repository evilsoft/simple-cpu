#ifndef COLOR_HPP
#define COLOR_HPP

#include <string>

extern const std::string CLR;
extern const std::string RST;

extern const std::string BLU;
extern const std::string GRN;
extern const std::string RED;
extern const std::string YEL;

extern const std::string BLD;
extern const std::string DIM;
extern const std::string UND;

extern const std::string CLS;

#endif
