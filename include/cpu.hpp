#ifndef CPU_HPP
#define CPU_HPP

#include "helpers.hpp"

class Bus;

struct CpuRegisters {
  data_t  a;
  data_t  x;
  data_t  y;

  addr_t  pc;
  uint8_t sp;

  uint8_t status;
  int cur_tick;
};

enum STATUS_FLAG {
  C = 0x01, // Carry
  Z = 0x02, // Zero
  I = 0x04, // Disable Interrupts
  D = 0x08, // Decimal
  B = 0x10, // Break
  V = 0x40, // Overflow
  N = 0x80, // Negative
};

class Cpu {
  public:
    Cpu();
    ~Cpu();

  private:
    Bus* bus;

    data_t a;
    data_t x;
    data_t y;
    addr_t  pc;
    uint8_t sp;
    uint8_t status;

    addr_t addr_bus;
    bool isWriting;
    uint8_t remaining;
    int tick;

    uint8_t branch(bool takeJmp);
    data_t fetch();
    addr_t fetchAddr();
    data_t peekData();
    data_t pop();
    Cpu* push(data_t value);
    addr_t readAddr();
    data_t readData();
    bool readFlag(STATUS_FLAG flag);

    Cpu* writeData(data_t data);
    Cpu* writeFlag(STATUS_FLAG flag, bool value);
    Cpu* writeZN(data_t value);

    bool hasCrossedPage(addr_t prev);

    struct OPCODE {
      uint8_t (Cpu::*operation)() = nullptr;
      uint8_t (Cpu::*mode)() = nullptr;
      uint8_t cycles;
      bool isWrite;
      OPCODE(uint8_t (Cpu::*op)(), uint8_t (Cpu::*m)(), uint8_t c, bool w = false)
        : operation(op), mode(m), cycles(c), isWrite(w) {}
    };

    // Opcode Lookup Table
    vector<OPCODE> opcodes;

    // Address Modes
    uint8_t abs(); uint8_t abx(); uint8_t aby();
    uint8_t imm(); uint8_t imp(); uint8_t izx();
    uint8_t izy(); uint8_t rel(); uint8_t zpg();
    uint8_t zpx(); uint8_t zpy();

    // Opcode Operations
    uint8_t AND(); uint8_t BCC(); uint8_t BCS(); uint8_t BEQ();
    uint8_t BMI(); uint8_t BNE(); uint8_t BPL(); uint8_t BRK();
    uint8_t CLC(); uint8_t CLD(); uint8_t CLI(); uint8_t CLV();
    uint8_t DEC(); uint8_t DEX(); uint8_t DEY(); uint8_t EOR();
    uint8_t INC(); uint8_t INX(); uint8_t INY(); uint8_t LDA();
    uint8_t LDX(); uint8_t LDY(); uint8_t ORA(); uint8_t PHA();
    uint8_t PHP(); uint8_t PLA(); uint8_t PLP(); uint8_t SEC();
    uint8_t SED(); uint8_t SEI(); uint8_t STA(); uint8_t STX();
    uint8_t STY(); uint8_t TAX(); uint8_t TAY(); uint8_t TSX();
    uint8_t TXA(); uint8_t TXS(); uint8_t TYA();
    uint8_t xxx();

  public:
    Cpu* ConnectBus(Bus *b);
    CpuRegisters ReadRegisters();

    Cpu* Reset();

    bool Tick();
};

#endif
