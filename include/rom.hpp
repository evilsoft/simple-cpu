#ifndef ROM_HPP
#define ROM_HPP

#include "helpers.hpp"

class Rom {
  public:
    Rom();
    ~Rom();

  private:
    array<data_t, 2 * 0x1000> rom;
    addr_t loc;

    void loadRom(string file);

  public:
    addr_t ConnectRom(string file);
    data_t ReadFrom(addr_t addr);
};

#endif
