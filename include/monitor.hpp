#ifndef MONITOR_HPP
#define MONITOR_HPP

#include "helpers.hpp"
#include "color.hpp"
#include "bus.hpp"
#include "cpu.hpp"

class Monitor {
  public:
    Monitor(Bus* b);
    ~Monitor();

  private:
    Bus* bus;
    string cmd;
    CpuRegisters cpuData;
    uint8_t page;
    data_t pageData[256];

    string sep;

    string flagHighlight(STATUS_FLAG flag, uint8_t status, char little, char big);
    Monitor* incPage();
    string markValue(bool mark, data_t value);
    Monitor* nextInstruction();
    bool nextTick();
    string renderCounterArea();
    string renderCpu();
    string renderOutput();
    string renderRamPage();
    string renderStack();
    Monitor* runToBreak();
    Monitor* setPage(uint8_t p);

  public:
    string ProcessInput(string input);
};

#endif
