TARGET = simple-cpu

CXX = g++
OPTS = -std=c++11 -O2 -Wall

BIN_DIR = bin
SRC_DIR = src
OBJ_DIR = build
INC_DIR = include
DAT_DIR = data

RST		= \033[0m
SUCC 	= \033[32m
INFO	= \033[36m
FAIL	= \033[31m
BOLD  = \033[1m

SRCS = $(wildcard $(SRC_DIR)/*.cpp)
DATA = $(wildcard $(DAT_DIR)/*.acme)
OBJS = $(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(SRCS))
PRGS = $(patsubst $(DAT_DIR)/%.acme,$(BIN_DIR)/$(DAT_DIR)/%.prg,$(DATA))

CXXFLAGS = -I$(INC_DIR) $(OPTS)

UNAME = $(shell uname)
ECHO_ARG = ""

ifeq ($(UNAME), Linux)
	ECHO_ARG = "-e"
endif

.PHONY: clean fresh run all

all: $(BIN_DIR)/$(TARGET) $(PRGS)

$(BIN_DIR)/$(TARGET): $(OBJS)
	@echo $(ECHO_ARG) "\n$(INFO)Creating Binary Directory$(RST)"
	@mkdir -p $(BIN_DIR)/$(DAT_DIR)
	@echo $(ECHO_ARG) "$(SUCC)$(BOLD)  ==>$(RST) $(BIN_DIR)"
	@echo $(ECHO_ARG) "\n$(INFO)Creating Build Target$(RST)"
	@$(CXX) $(CXXFLAGS) $^ -o $@
	@echo $(ECHO_ARG) "$(SUCC)$(BOLD)  ==>$(RST) $@"

$(BIN_DIR)/$(DAT_DIR)/%.prg: $(DAT_DIR)/%.acme
	@echo $(ECHO_ARG) "$(INFO)Building:$(RST) $<"
	@acme -o $@ -f cbm $<
	@echo $(ECHO_ARG) "$(SUCC)$(BOLD)  ==>$(RST) $@"

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp $(OBJ_DIR)/.f
	@echo $(ECHO_ARG) "$(INFO)Building:$(RST) $<"
	@$(CXX) $(CXXFLAGS) -c $< -o $@
	@echo $(ECHO_ARG) "$(SUCC)$(BOLD)  ==>$(RST) $@"

$(OBJ_DIR)/.f:
	@echo $(ECHO_ARG) "\n$(INFO)Creating Object Directory$(RST)"
	@mkdir -p $(OBJ_DIR)
	@touch $(OBJ_DIR)/.f
	@echo $(ECHO_ARG) "$(SUCC)$(BOLD)  ==>$(RST) $(OBJ_DIR)\n"

clean:
	@echo $(ECHO_ARG) "\n$(INFO)Removing Build Directories$(RST)"
	@rm -rf $(BIN_DIR) $(OBJ_DIR)
	@echo $(ECHO_ARG) "$(SUCC)$(BOLD)  ==>$(RST) $(BIN_DIR)"
	@echo $(ECHO_ARG) "$(SUCC)$(BOLD)  ==>$(RST) $(OBJ_DIR)"

fresh: clean all

run: all
	@echo $(ECHO_ARG) "\n$(INFO)Running Program:$(RST) $(TARGET)\n"
	cd $(BIN_DIR); ./$(TARGET)
