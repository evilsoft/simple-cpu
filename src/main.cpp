#include "helpers.hpp"

#include "bus.hpp"
#include "monitor.hpp"

Bus* bus = nullptr;
Monitor* mon = nullptr;

Monitor* initialize() {
  bus = new Bus();
  mon = new Monitor(bus);

  return mon;
}

int cleanup() {
  delete mon;
  delete bus;

  bus = nullptr;
  mon = nullptr;

  return 0;
}

int main() {
  Monitor* m = initialize();

  bool done = false;

  cout << m->ProcessInput("");

  while(!done) {
    string input;
    cout << BLU << "> " << CLR;
    getline(cin, input);

    // dat ctrl+d doh..
    if(cin.eof())
      return cleanup();

    string output = m->ProcessInput(input);

    cout << output;

    if(output == "") {
      done = true;
    }
  }

  return cleanup();
}
