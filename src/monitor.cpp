#include "monitor.hpp"

// Construction
/////////////////////////////////////////////////////

Monitor::Monitor(Bus* b) {
  stringstream ss;
  ss << YEL << string(57, '=') << CLR << endl;

  cmd   = "";
  sep   = ss.str();
  bus   = b;
  page  = 0x00;

  // alter break/interrupt handler
  bus
    ->WriteTo(0xfffe, 0x00)
    ->WriteTo(0xffff, 0xff);
}

Monitor::~Monitor() {
}

// Private Methods
/////////////////////////////////////////////////////

Monitor* Monitor::incPage() {
  uint8_t p = page == 0xff
    ? 0x00 : page + 1;

  setPage(p);

  return this;
}

string Monitor::flagHighlight(STATUS_FLAG flag, uint8_t status, char little, char big) {
  return (flag & status) > 0
    ? (UND + BLD) + big
    : DIM + little;
}

string Monitor::markValue(bool mark, data_t value) {
  stringstream ss;

  if(mark)
    ss << GRN << HEX(value) << RST;
  else
    ss << HEX(value);

  return ss.str();
}

Monitor* Monitor::nextInstruction() {
  while(!nextTick()) {}
  return this;
}

bool Monitor::nextTick() {
  return bus->Tick();
}

string Monitor::renderCounterArea() {
  addr_t loc;
  stringstream ss;
  data_t value;

  addr_t current = bus->ReadCpuRegisters().pc;
  addr_t start = (current & 0xfff0) - 0x0010;

  for(uint8_t row = 0; row <= 2; row++) {
    ss << "  ";
    ss << BLU << HEX_ADDR((0xffff & (start + (row * 0x10)))) << ":" << CLR;

    for(uint8_t offset = 0; offset <= 0x0f; offset++) {
      loc = start + (row * 0x10) + offset;
      value = bus->ReadFrom(loc);
      ss << " " << markValue(loc == current, value);
    }

    ss << endl;
  }

  return ss.str();
}

string Monitor::renderCpu() {
  CpuRegisters regs = bus->ReadCpuRegisters();
  stringstream ss;

  ss << "   ";
  ss << BLU << "A: " << CLR;
  ss << markValue(regs.a != cpuData.a, regs.a);

  ss << BLU << "     X: " << CLR;
  ss << markValue(regs.x != cpuData.x, regs.x);

  ss << BLU << "     Y: " << CLR;
  ss << markValue(regs.y != cpuData.y, regs.y);

  ss << BLU << "     T: " << CLR;
  ss << setfill('0') << setw(4) << +regs.cur_tick;

  ss << endl;

  ss << "  ";
  ss << BLU << "PC: " << CLR;
  ss << HEX_ADDR(regs.pc);

  ss << BLU << "  SP: " << CLR;
  ss << "01" << markValue(regs.sp != cpuData.sp, regs.sp);

  ss << "  ";
  ss << " " << flagHighlight(N, regs.status, 'n', 'N') << CLR;
  ss << " " << flagHighlight(V, regs.status, 'v', 'V') << CLR;
  ss << " -";
  ss << " " << flagHighlight(B, regs.status, 'b', 'B') << CLR;
  ss << " " << flagHighlight(D, regs.status, 'd', 'D') << CLR;
  ss << " " << flagHighlight(I, regs.status, 'i', 'I') << CLR;
  ss << " " << flagHighlight(Z, regs.status, 'z', 'Z') << CLR;
  ss << " " << flagHighlight(C, regs.status, 'c', 'C') << CLR;

  ss << endl;

  cpuData = regs;

  return ss.str();
}

string Monitor::renderOutput() {
  stringstream ss;

  // Clear Terminal
  ss << CLS << sep;

  // Render CPU Data
  ss << renderCpu() << sep;

  // Render Counter Area
  ss << renderStack() << sep;

  // Render Counter Area
  ss << renderCounterArea() << sep;

  // Render Current RAM Page
  ss << renderRamPage() << sep;

  return ss.str();
}

string Monitor::renderRamPage() {
  stringstream ss;

  addr_t loc;
  addr_t start;
  data_t value;

  addr_t hi = (page<<8);

  for(int row = 0; row <= 0x0f; row++) {
    start = hi + (row<<4);

    // Print Row Label
    ss << "  ";
    ss << BLU << HEX_ADDR(start) << ":" << CLR;

    // Render Row Data
    for(uint8_t offset = 0; offset <= 0x0f; offset++) {
      loc = start + offset;
      value = bus->ReadFrom(loc);

      ss << " ";
      ss << markValue(pageData[loc & 0xff] != value, value);

      pageData[loc & 0xff] = value;
    }

    ss << endl;
  }

  return ss.str();
}

string Monitor::renderStack() {
  addr_t loc;
  stringstream ss;
  data_t value;

  uint8_t current = bus->ReadCpuRegisters().sp;
  uint8_t start = (current & 0xf0) - 0x10;

  for(uint8_t row = 0; row <= 1; row++) {
    addr_t row_start = 0x0100 | (start + (row * 0x10));

    ss << "  ";
    ss << BLU << HEX_ADDR(row_start) << ":" << CLR;

    for(uint8_t offset = 0; offset <= 0x0f; offset++) {
      loc =  row_start + offset;
      value = bus->ReadFrom(loc);
      ss << " " << markValue(loc == (0x0100 | current), value);
    }

    ss << endl;
  }

  return ss.str();
}

Monitor* Monitor::runToBreak() {
  while(bus->ReadCpuRegisters().pc != 0xff00)
    nextTick();

  return this;
}

Monitor* Monitor::setPage(uint8_t p) {
  page = p;
  addr_t start = (page<<8);

  for(int offset = 0; offset <= 0xff; offset++)
    pageData[offset] = bus->ReadFrom(start + offset);

  return this;
}

// Public Methods
/////////////////////////////////////////////////////

string Monitor::ProcessInput(string input) {
  vector<string> result;

  if(input != "") {
    istringstream iss(input);

    for(string s; iss >> s;)
      result.push_back(s);

    cmd = result[0];
  }

  if(cmd != "") {
    // Page View (p [hex page] -- 'p 5e')
    // no arg inc page
    if(cmd == "p") {
      if(result.size() > 1)
        setPage(hexStringToInteger(result[1]));
      else
        incPage();
    }

    // Clock Tick
    if(cmd == "t")
      nextTick();

    // Next Instruction
    if(cmd == "n")
      nextInstruction();

    // Next Instruction
    if(cmd == "r")
      runToBreak();

    // Quit
    if(cmd == "q")
      return "";
  }

  return renderOutput();
}
