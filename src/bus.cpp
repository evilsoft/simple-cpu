#include "bus.hpp"

// Construction
/////////////////////////////////////////////////////

Bus::Bus() {
  rom = new Rom();
  cpu = new Cpu();

  // Initialize RAM
  for(auto &i : ram)
    i = 0x00;

  // Load Kernal ROM
  romStart = rom->ConnectRom("./data/kernel.prg");

  // Set Reset Vector
  ram[0xfffc] = romStart & 0xff;
  ram[0xfffd] = romStart >> 8;

  // Connect and Reset CPU
  cpu->ConnectBus(this)
    ->Reset();
}

Bus::~Bus() {
  delete rom;
  delete cpu;
}

// Public Methods
/////////////////////////////////////////////////////

CpuRegisters Bus::ReadCpuRegisters() {
  return cpu->ReadRegisters();
}

data_t Bus::ReadFrom(addr_t addr) {
  if(addr >= romStart && addr < 0xfffa) {
    return rom->ReadFrom(addr - romStart);
  }

  return ram[addr];
}

bool Bus::Tick() {
  return cpu->Tick();
}

Bus* Bus::WriteTo(addr_t addr, data_t data) {
  if(addr >= romStart && addr < 0xfffa) {
    return this;
  }

  ram[addr] = data;

  return this;
}
