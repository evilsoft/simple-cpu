#include "cpu.hpp"
#include "bus.hpp"

// Construction
/////////////////////////////////////////////////////

Cpu::Cpu() {
  using a = Cpu;

  opcodes = {
    // 0x00
    { &a::BRK, &a::imp, 7 }, { &a::ORA, &a::izx, 6 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::ORA, &a::zpg, 3 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::PHP, &a::imp, 3 }, { &a::ORA, &a::imm, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::ORA, &a::abs, 4 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },

    // 0x10
    { &a::BPL, &a::rel, 2 }, { &a::ORA, &a::izy, 5 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::ORA, &a::zpx, 4 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::CLC, &a::imp, 2 }, { &a::ORA, &a::aby, 4 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::ORA, &a::abx, 4 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },

    // 0x20
    { &a::xxx, &a::imp, 2 }, { &a::AND, &a::izx, 6 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::AND, &a::zpg, 3 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::PLP, &a::imp, 4 }, { &a::AND, &a::imm, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::AND, &a::abs, 4 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },

    // 0x30
    { &a::BMI, &a::rel, 2 }, { &a::AND, &a::izy, 5 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::AND, &a::zpx, 4 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::SEC, &a::imp, 2 }, { &a::AND, &a::aby, 4 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::AND, &a::abx, 4 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },

    // 0x40
    { &a::xxx, &a::imp, 2 }, { &a::EOR, &a::izx, 6 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::EOR, &a::zpg, 3 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::PHA, &a::imp, 3 }, { &a::EOR, &a::imm, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::EOR, &a::abs, 4 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },

    // 0x50
    { &a::xxx, &a::imp, 2 }, { &a::EOR, &a::izy, 5 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::EOR, &a::zpx, 4 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::CLI, &a::imp, 2 }, { &a::EOR, &a::aby, 4 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::EOR, &a::abx, 4 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },

    // 0x60
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::PLA, &a::imp, 4 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },

    // 0x70
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::SEI, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },

    // 0x80
    { &a::xxx, &a::imp, 2 }, { &a::STA, &a::izx, 6, true }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::STY, &a::zpg, 3, true }, { &a::STA, &a::zpg, 3, true }, { &a::STX, &a::zpg, 3, true }, { &a::xxx, &a::imp, 2 },
    { &a::DEY, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::TXA, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::STY, &a::abs, 4, true }, { &a::STA, &a::abs, 4, true }, { &a::STX, &a::abs, 4, true }, { &a::xxx, &a::imp, 2 },

    // 0x90
    { &a::BCC, &a::rel, 2 }, { &a::STA, &a::izy, 6, true }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::STY, &a::zpx, 4, true }, { &a::STA, &a::zpx, 4, true }, { &a::STX, &a::zpy, 4, true }, { &a::xxx, &a::imp, 2 },
    { &a::TYA, &a::imp, 2 }, { &a::STA, &a::aby, 5, true }, { &a::TXS, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::STA, &a::abx, 5, true }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },

    // 0xa0
    { &a::LDY, &a::imm, 2 }, { &a::LDA, &a::izx, 6 }, { &a::LDX, &a::imm, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::LDY, &a::zpg, 3 }, { &a::LDA, &a::zpg, 3 }, { &a::LDX, &a::zpg, 3 }, { &a::xxx, &a::imp, 2 },
    { &a::TAY, &a::imp, 2 }, { &a::LDA, &a::imm, 2 }, { &a::TAX, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::LDY, &a::abs, 4 }, { &a::LDA, &a::abs, 4 }, { &a::LDX, &a::abs, 4 }, { &a::xxx, &a::imp, 2 },

    // 0xb0
    { &a::BCS, &a::rel, 2 }, { &a::LDA, &a::izy, 5 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::LDY, &a::zpx, 4 }, { &a::LDA, &a::zpx, 4 }, { &a::LDX, &a::zpy, 4 }, { &a::xxx, &a::imp, 2 },
    { &a::CLV, &a::imp, 2 }, { &a::LDA, &a::aby, 4 }, { &a::TSX, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::LDY, &a::abx, 4 }, { &a::LDA, &a::abx, 4 }, { &a::LDX, &a::aby, 4 }, { &a::xxx, &a::imp, 2 },

    // 0xc0
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::DEC, &a::zpg, 5, true }, { &a::xxx, &a::imp, 2 },
    { &a::INY, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::DEX, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::DEC, &a::abs, 6, true }, { &a::xxx, &a::imp, 2 },

    // 0xd0
    { &a::BNE, &a::rel, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::DEC, &a::zpx, 6, true }, { &a::xxx, &a::imp, 2 },
    { &a::CLD, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::DEC, &a::abx, 7, true }, { &a::xxx, &a::abx, 2 },

    // 0xe0
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::INC, &a::zpg, 5, true }, { &a::xxx, &a::imp, 2 },
    { &a::INX, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::INC, &a::abs, 6, true }, { &a::xxx, &a::imp, 2 },

    // 0xf0
    { &a::BEQ, &a::rel, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::INC, &a::zpx, 6, true }, { &a::xxx, &a::imp, 2 },
    { &a::SED, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 },
    { &a::xxx, &a::imp, 2 }, { &a::xxx, &a::imp, 2 }, { &a::INC, &a::abx, 7, true }, { &a::xxx, &a::imp, 2 },
  };

  remaining = 0;
  addr_bus = 0x0000;
  tick = 0;
}

Cpu::~Cpu() {
}

// Private Methods
/////////////////////////////////////////////////////

uint8_t Cpu::branch(bool takeJmp) {
  if(!takeJmp)
    return 0;

  addr_t prev = pc - 1;
  pc = addr_bus;

  return 1 + hasCrossedPage(prev);
}

// Reads a byte from instructions, inc pc (1)
// and transposes
data_t Cpu::fetch() {
  return bus->ReadFrom(pc++);
}

// Reads address from instructions, inc pc (2)
// and transposes
addr_t Cpu::fetchAddr() {
  return fetch() | (fetch()<<8);
}

// Reads location from memory, does not touch addr_bus
data_t Cpu::peekData() {
  return bus->ReadFrom(addr_bus);
}

// pop value off of stack, inc stack pointer
data_t Cpu::pop() {
  addr_bus = 0x0100 | ++sp;
  return peekData();
}

// push value onto stack, dec stack pointer
Cpu* Cpu::push(data_t value) {
  addr_bus = 0x0100 | sp--;
  writeData(value);

  return this;
}

// Reads address from memory, inc addr_bus (2)
// and transposes
addr_t Cpu::readAddr() {
  return readData() | (readData() << 8);
}

// Reads location from memory, inc addr_bus (1)
data_t Cpu::readData() {
  return bus->ReadFrom(addr_bus++);
}

// Reads single status flag
bool Cpu::readFlag(STATUS_FLAG flag) {
  return ((status & flag) > 0);
}

// Writes data to address_bus, inc addr_bus (1)
Cpu* Cpu::writeData(data_t data) {
  bus->WriteTo(addr_bus++, data);

  return this;
}

// Sets a given flag
Cpu* Cpu::writeFlag(STATUS_FLAG flag, bool value) {
  if(value)
    status |= flag;
  else
    status &= ~flag;

  return this;
}

// Helper to set Z and N flags
Cpu* Cpu::writeZN(data_t value) {
  writeFlag(Z, value == 0x00);
  writeFlag(N, value & 0x80);

  return this;
}

bool Cpu::hasCrossedPage(addr_t prev) {
  return !isWriting
    && (prev & 0xff00) != (addr_bus & 0xff00);
}

// Address modes
/////////////////////////////////////////////////////

uint8_t Cpu::abs() {
  addr_bus = fetchAddr();

  return 0;
}

uint8_t Cpu::abx() {
  addr_t orig = fetchAddr();
  addr_bus = orig + x;

  return hasCrossedPage(orig);
}

uint8_t Cpu::aby() {
  addr_t orig = fetchAddr();
  addr_bus = orig + y;

  return hasCrossedPage(orig);
}

uint8_t Cpu::imm() {
  addr_bus = pc++;

  return 0;
}

uint8_t Cpu::imp() {
  return 0;
}

uint8_t Cpu::izx() {
  addr_bus = (fetch() + x) & 0xff;
  addr_bus = readAddr();

  return 0;
}

uint8_t Cpu::izy() {
  addr_bus = fetch();
  addr_t orig = readAddr();
  addr_bus = orig + y;

  return hasCrossedPage(orig);
}

uint8_t Cpu::rel() {
  addr_bus = (pc + 1) + (int8_t)fetch();

  return 0;
}

uint8_t Cpu::zpg() {
  addr_bus = fetch();

  return 0;
}

uint8_t Cpu::zpx() {
  addr_bus = (fetch() + x) & 0xff;

  return 0;
}

uint8_t Cpu::zpy() {
  addr_bus = (fetch() + y) & 0xff;

  return 0;
}

// Opcodes
/////////////////////////////////////////////////////

uint8_t Cpu::AND() {
  a &= peekData();
  writeZN(a);

  return 0;
}

uint8_t Cpu::BCC() {
  return branch(readFlag(C));
}

uint8_t Cpu::BCS() {
  return branch(!readFlag(C));
}

uint8_t Cpu::BEQ() {
  return branch(readFlag(Z));
}

uint8_t Cpu::BMI() {
  return branch(readFlag(N));
}

uint8_t Cpu::BNE() {
  return branch(!readFlag(Z));
}

uint8_t Cpu::BPL() {
  return branch(!readFlag(N));
}

uint8_t Cpu::BRK() {
  addr_t ret = pc + 1;

  push(ret >> 8)
    ->push(ret & 0xff)
    ->push(status);

  addr_bus = 0xfffe;
  pc = readAddr();

  return 0;
}

uint8_t Cpu::CLC() {
  writeFlag(C, false);

  return 0;
}

uint8_t Cpu::CLD() {
  writeFlag(D, false);

  return 0;
}

uint8_t Cpu::CLI() {
  writeFlag(I, false);
  return 0;
}

uint8_t Cpu::CLV() {
  writeFlag(V, false);
  return 0;
}

uint8_t Cpu::DEC() {
  uint8_t temp = peekData() - 1;

  writeData(temp);
  writeZN(temp);

  return 0;
}

uint8_t Cpu::DEX() {
  writeZN(--x);

  return 0;
}

uint8_t Cpu::DEY() {
  writeZN(--y);

  return 0;
}

uint8_t Cpu::EOR() {
  a ^= peekData();
  writeZN(a);

  return 0;
}

uint8_t Cpu::INC() {
  uint8_t temp = peekData() + 1;

  writeData(temp);
  writeZN(temp);

  return 0;
}

uint8_t Cpu::INX() {
  writeZN(++x);

  return 0;
}

uint8_t Cpu::INY() {
  writeZN(++y);

  return 0;
}

uint8_t Cpu::LDA() {
  a = readData();
  writeZN(a);

  return 0;
}

uint8_t Cpu::LDX() {
  x = readData();
  writeZN(x);

  return 0;
}

uint8_t Cpu::LDY() {
  y = readData();
  writeZN(y);

  return 0;
}

uint8_t Cpu::ORA() {
  a |= peekData();
  writeZN(a);

  return 0;
}

uint8_t Cpu::PHA() {
  push(a);

  return 0;
}

uint8_t Cpu::PHP() {
  push(status);

  return 0;
}

uint8_t Cpu::PLA() {
  a = pop();
  writeZN(a);

  return 0;
}

uint8_t Cpu::PLP() {
  status = pop();

  return 0;
}

uint8_t Cpu::SEC() {
  writeFlag(C, true);

  return 0;
}

uint8_t Cpu::SED() {
  writeFlag(D, true);

  return 0;
}

uint8_t Cpu::SEI() {
  writeFlag(I, true);
  return 0;
}

uint8_t Cpu::STA() {
  writeData(a);

  return 0;
}

uint8_t Cpu::STX() {
  writeData(x);

  return 0;
}

uint8_t Cpu::STY() {
  writeData(y);

  return 0;
}

uint8_t Cpu::TAX() {
  x = a;
  writeZN(x);
  return 0;
}

uint8_t Cpu::TAY() {
  y = a;
  writeZN(y);
  return 0;
}

uint8_t Cpu::TSX() {
  x = sp;
  writeZN(x);
  return 0;
}

uint8_t Cpu::TXA() {
  a = x;
  writeZN(a);

  return 0;
}

uint8_t Cpu::TXS() {
  sp = x;
  return 0;
}

uint8_t Cpu::TYA() {
  a = y;
  writeZN(a);
  return 0;
}

uint8_t Cpu::xxx() {
  return 0;
}

// Public Methods
/////////////////////////////////////////////////////

Cpu* Cpu::ConnectBus(Bus *b) {
  bus = b;
  return this;
}

CpuRegisters Cpu::ReadRegisters() {
  CpuRegisters regs;

  regs.a = a;
  regs.x = x;
  regs.y = y;

  regs.pc = pc;
  regs.sp = sp;

  regs.status = status;

  regs.cur_tick = tick;

  return regs;
}

Cpu* Cpu::Reset() {
  // set registers
  a = y = x = 0x00;

  sp = 0x00;
  status = 0x30;

  addr_bus = 0xfffc;

  // Read Reset Location
  pc = readAddr();

  return this;
}

bool Cpu::Tick() {
  bool complete = false;

  if(remaining == 0) {
    data_t opcode = bus->ReadFrom(pc);
    pc++;

    OPCODE op = opcodes[opcode];

    isWriting = op.isWrite;
    remaining = op.cycles;
    remaining += (this->*opcodes[opcode].mode)();
    remaining += (this->*opcodes[opcode].operation)();
  } else if(remaining == 1) {
    complete = true;
  }

  tick++;
  remaining--;

  return complete;
}
