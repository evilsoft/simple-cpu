#include "rom.hpp"

// Construction
/////////////////////////////////////////////////////

Rom::Rom () {
}

Rom::~Rom () {
}

// Private Methods
/////////////////////////////////////////////////////

void Rom::loadRom(string file) {
  ifstream f(file, ios::in|ios::binary|ios::ate);

  if(f.is_open()) {
    size_t size = (int)f.tellg() - 2;

    char* addr = new char[2];
    char* data = new char [size];

    f.seekg(0, ios::beg);
    f.read(addr, 2);
    f.read(data, size);

    f.close();

    loc = (addr[0] & 0xff) | (addr[1]<<8);
    memcpy(&rom, data, size);

    delete[] addr;
    delete[] data;
  }
}

// Public Methods
/////////////////////////////////////////////////////

addr_t Rom::ConnectRom(string file) {
  loadRom(file);
  return loc;
}

data_t Rom::ReadFrom(addr_t addr) {
  return rom[addr];
}
