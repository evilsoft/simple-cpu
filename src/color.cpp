#include "color.hpp"

const std::string CLR = "\033[0m";
const std::string RST = "\033[37m";

const std::string BLU = "\033[34m";
const std::string GRN = "\033[32m";
const std::string RED = "\033[31m";
const std::string YEL = "\033[33m";

const std::string BLD = "\033[1m";
const std::string DIM = "\033[2m";
const std::string UND = "\033[4m";

const std::string CLS = "\033[2J";
