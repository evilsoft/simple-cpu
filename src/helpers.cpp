#include "helpers.hpp"

int hexStringToInteger(string hexString) {
  if(hexString.find_first_not_of("0123456789abcdefABCDEF", 2) == string::npos) {
    return stoi(hexString, nullptr, 16);
  }

  return 0x00;
}
